package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;


public class MayonnaiseSauceTest {
    private MayonnaiseSauce mayonnaiseSauce;

    @Before
    public void setUp() {
        mayonnaiseSauce = new MayonnaiseSauce();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Mayonnaise Sauce", mayonnaiseSauce.toString());
    }
}
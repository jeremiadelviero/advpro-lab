package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;


public class CreamCheeseTest {
    private CreamCheese creamCheese;

    @Before
    public void setUp() {
        creamCheese = new CreamCheese();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Cream Cheese", creamCheese.toString());
    }
}

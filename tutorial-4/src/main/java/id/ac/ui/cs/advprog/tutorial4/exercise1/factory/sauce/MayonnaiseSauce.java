package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class MayonnaiseSauce implements Sauce {
    public String toString() {
        return "Mayonnaise Sauce";
    }
}
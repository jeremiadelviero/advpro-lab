package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class GreenClams implements Clams {

    public String toString() {
        return "Green Clams from Tanjung Priok";
    }
}

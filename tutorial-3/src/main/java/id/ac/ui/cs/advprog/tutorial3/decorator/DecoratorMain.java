package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorMain {
    public static void main(String[] args) {
        Food ThinBunBurgerWithMixedMeat;
        ThinBunBurgerWithMixedMeat = BreadProducer.THIN_BUN.createBreadToBeFilled();
        ThinBunBurgerWithMixedMeat = FillingDecorator.BEEF_MEAT.addFillingToBread(ThinBunBurgerWithMixedMeat);
        ThinBunBurgerWithMixedMeat = FillingDecorator.CHICKEN_MEAT.addFillingToBread(ThinBunBurgerWithMixedMeat);
        System.out.println("Thin Bun Burger with Mixed Meat description: " + ThinBunBurgerWithMixedMeat.getDescription());
        System.out.println("Thin Bun Burger with Mixed Meat cost: " + ThinBunBurgerWithMixedMeat.cost());

        Food NoCrustSandwichWithChickenNCheese;
        NoCrustSandwichWithChickenNCheese = BreadProducer.NO_CRUST_SANDWICH.createBreadToBeFilled();
        NoCrustSandwichWithChickenNCheese = FillingDecorator.CHICKEN_MEAT.addFillingToBread(NoCrustSandwichWithChickenNCheese);
        NoCrustSandwichWithChickenNCheese = FillingDecorator.CHEESE.addFillingToBread(NoCrustSandwichWithChickenNCheese);
        System.out.println("No Crust Sandwich with Chicken Meat and Cheese description: " + NoCrustSandwichWithChickenNCheese);
        System.out.println("No Crust Sandwich with Chicken Meat and Cheese cost: " + NoCrustSandwichWithChickenNCheese);
    }
}

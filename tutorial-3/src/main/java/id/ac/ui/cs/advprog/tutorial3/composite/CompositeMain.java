package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;

import java.util.List;

public class CompositeMain {
    public static void main(String[] args) {
        Company company = new Company();
        Employees ceo = new Ceo("Johann Samuel", 1000000.00);
        Employees cto = new Cto("Jeremiah Delviero", 500000.00);
        Employees frontendProgrammer = new FrontendProgrammer("Jessica Stan", 75000.00);
        company.addEmployee(ceo);
        company.addEmployee(cto);
        company.addEmployee(frontendProgrammer);
        List<Employees> allEmployees = company.getAllEmployees();
        System.out.print("This company members: ");
        for (Employees employee : allEmployees) {
            System.out.print(employee.getName() + " as " + employee.getRole() + " with salary = " + employee.getSalary() + ", ");
        }
        System.out.println();
        System.out.println("This company net salaries: " + company.getNetSalaries());
    }
}
